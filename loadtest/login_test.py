import requests
from locust import HttpUser, TaskSet, task

URL = 'http://wp.team1800.chroot.name/'


class UserBehavior(HttpUser):
#    headers = {'Authorization': 'Bearer cff2648ce5c0c079ba26f8f0ff10fa49a25670c281377cd8348a93cf6ae94b230c82273bd6caee80f6ba61585c1adc6931c5bdade78da5f7f80d5b29337d'}

#    @task
#    def users_login(self):
#        json_data = {
#            "login": "test",
#            "password": "test"
#        }
#        self.client.post('users/login', json=json_data)


    @task
    def home(self):
        self.client.get('/')

    @task
    def myfirstpost(self):
        self.client.get('/2023/06/20/my-first-post')

    @task
    def helloworld(self):
        self.client.get('/2023/06/15/hello-world')

    @task
    def wpadmin(self):
        self.client.get('/wp-admin')






#class WebsiteGetPrices(HttpUser):
#    task_set = UserBehavior()
#    min_wait = 1000
#    max_wait = 2000
