#!/bin/bash

BACKUP_DIR=/mnt/backup/
BACKUP_TEMP_DIR=/mnt/backup/mysql/temp
BACKUP_ARCHIVES_DIR=/mnt/backup/mysql/archives
#LOG_FILE=/root/scripts/backup/mysql-backup-"$(date +%F-%H%M%S)".log

[[ -d $BACKUP_DIR ]] || mkdir -p $BACKUP_DIR

echo "
**********
Монтирование NFS
**********
"

[[ -d $BACKUP_DIR ]] && mount -t nfs 10.0.0.9:/shares/share-54bc0a8b-cd91-4c2f-b9d9-495faf1a4b6e $BACKUP_DIR

mountpoint -q $BACKUP_DIR && echo "NFS примонтирован"

echo "
**********
Очищаем временную папку бекапов
**********
"
rm -rf $BACKUP_TEMP_DIR
[[ -d $BACKUP_TEMP_DIR ]] || mkdir -p $BACKUP_TEMP_DIR

echo "Cоздаём бекап"
xtrabackup --user=wordpressadmin \
 --password=passwd2023 \
 --databases=wordpress \
 --host=localhost \
 --port=3306 \
 --backup \
 --target-dir=$BACKUP_TEMP_DIR

echo "
**********
Выполняем подготовку бекапа для развёртывания
**********
"
xtrabackup --prepare --target-dir=$BACKUP_TEMP_DIR

echo "
**********
Создаём архив
**********
"
[[ -d $BACKUP_ARCHIVES_DIR ]] || mkdir -p $BACKUP_ARCHIVES_DIR
find $BACKUP_ARCHIVES_DIR -type f -mtime +30 -name "*.tar.gz" -delete
tar -zcvf $BACKUP_ARCHIVES_DIR/xtrabackup-wordpress-"$(date +%F-%H%M%S)".tar.gz $BACKUP_TEMP_DIR && rm -rf $BACKUP_TEMP_DIR

echo "
**********
Отмонтирование NFS
**********
"
umount $BACKUP_DIR
mountpoint -q $BACKUP_DIR || echo "NFS отмонтирован"