#!/bin/bash

echo "Установить Python3"
sudo apt install python3 python-is-python3 -y

echo "Создать пользователя ubuntu, добавить ssh-ключ для доступа"
USERNAME="ubuntu"
SSH_KEY="ssh-rsa ..."
useradd -m -s /bin/bash -r $USERNAME
mkdir /home/$USERNAME/.ssh
echo "$SSH_KEY" > /home/$USERNAME/.ssh/authorized_keys
chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh
chmod 700 /home/$USERNAME/.ssh
chmod 400 /home/$USERNAME/.ssh/authorized_keys
usermod -aG sudo $USERNAME

echo "
Добавить права на беспарольное исполнение команд пользователю ubuntu.
Внести строку в файл /etc/sudoers через команду visudo
ubuntu ALL=(ALL) NOPASSWD: ALL
"