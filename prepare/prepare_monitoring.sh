#!/bin/bash

if [[ ! -f /usr/local/bin/docker-compose ]]; then
  curl -L https://github.com/docker/compose/releases/download/v2.19.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
  chmod +x /usr/local/bin/docker-compose
fi

CV="$(docker-compose --version | awk -F "version v" '{print $2}')"
if [ $CV == "2.19.0" ];
then
    docker-compose --version
else
    echo "docker-compose version is not allowed"
fi