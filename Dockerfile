FROM alpine:3.17

RUN apk update && \
    apk add py3-pip

RUN pip3 install ansible

COPY ansible.cfg /etc/ansible/ansible.cfg
COPY * /ansible/
WORKDIR /ansible

CMD ["/bin/sh"]
